import pandas as pd
import geopandas as gpd

import branca.colormap as cm

import glob

import folium
from folium import Choropleth

# Function for displaying the map
def embed_map(m, file_name):
    from IPython.display import IFrame
    m.save(file_name)

#Read yellow taxis data from 2018

#If we want all the data from 2018 we use
#yellow_taxis = pd.concat([pd.read_csv(f) for f in glob.glob('flash/NYCTaxiData/yellow_taxi/2018/*.csv')], ignore_index = True)

#If we only want one month
yellow_taxis = pd.read_csv('yellow_tripdata_2018-01.csv')

yellow_taxis["PULocationID"] = yellow_taxis["PULocationID"].fillna(-1).astype(int).astype(str)
yellow_taxis["DOLocationID"] = yellow_taxis["DOLocationID"].fillna(-1).astype(int).astype(str)
print(yellow_taxis.head())

#VendorID is not an identifier of the taxi because there are only three possible values for it
yellow_taxis["VendorID"].value_counts()

#We only get the pick ups and drop offs regions
yellow_taxis_PU_DO = yellow_taxis[["PULocationID","DOLocationID"]]
print(yellow_taxis_PU_DO.head())

# Read location ID regions
taxi_zones = gpd.read_file("taxi_zones/taxi_zones.shp")
taxi_zones["location_i"] = taxi_zones["location_i"].astype(int).astype(str)
taxi_zones_named = taxi_zones[['location_i','zone', 'geometry']].rename(columns={"location_i": "LocationID"}).dissolve(by='LocationID')
print(taxi_zones_named.head())

#We create the df for the representations
taxi_pu_do = taxi_zones_named.reset_index()
taxi_pu_do['pick_ups'] = taxi_pu_do.LocationID.map(yellow_taxis_PU_DO.PULocationID.value_counts())
taxi_pu_do['drop_offs'] = taxi_pu_do.LocationID.map(yellow_taxis_PU_DO.DOLocationID.value_counts())
taxi_pu_do['pick_ups'] = taxi_pu_do['pick_ups'].fillna(0).astype(int)
taxi_pu_do['drop_offs'] = taxi_pu_do['drop_offs'].fillna(0).astype(int)
print(taxi_pu_do.head())

#We add a column for the difference between PU and DO
taxi_pu_do['diff'] = taxi_pu_do['pick_ups'] - taxi_pu_do['drop_offs']
taxi_pu_do['pu_norm'] = round(taxi_pu_do['pick_ups']/taxi_pu_do['pick_ups'].max(),6)
taxi_pu_do['do_norm'] = round(taxi_pu_do['drop_offs']/taxi_pu_do['drop_offs'].max(),6)

def diff_norm(df):
    z = []
    for index, row in df.iterrows():
        if row['diff'] != 0 :
            z.append(round(row['diff']/max(row['pick_ups'],row['drop_offs']),6))
        else :
            z.append(0.000000)
    return z

taxi_pu_do['diff_norm'] = diff_norm(taxi_pu_do)

print(taxi_pu_do.head())

print(taxi_pu_do.describe())

#We want to center our map properly, so :
x_map = taxi_zones.centroid.x.mean()
y_map = taxi_zones.centroid.y.mean()
print(x_map,y_map)

#Linear color map with steps based on quantiles
colormap1 = cm.linear.Blues_09.to_step(10).scale(0,1)
print(colormap1)

#We display the map
mymap1 = folium.Map(location=[y_map, x_map], zoom_start=11,tiles=None)
folium.TileLayer('CartoDB positron',name="Light Map",control=False).add_to(mymap1)
colormap1.caption = "Taxi Pick Ups"
style_function = lambda x: {"weight":0.5,
                            'color':'black',
                            'fillColor':colormap1(x['properties']['pu_norm']),
                            'fillOpacity':0.75}
highlight_function = lambda x: {'fillColor': '#000000',
                                'color':'#000000',
                                'fillOpacity': 0.50,
                                'weight': 0.1}
TaxiPU=folium.features.GeoJson(
        taxi_pu_do,
        style_function=style_function,
        control=False,
        highlight_function=highlight_function,
        tooltip=folium.features.GeoJsonTooltip(fields=['zone','pick_ups'],
            aliases=['Neighbourhood: ','Pick Ups: '],
            style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;"),
            sticky=True
        )
    )
colormap1.add_to(mymap1)
mymap1.add_child(TaxiPU)
embed_map(mymap1, 'mymap1.html')

#Como podemos observar, la mayoria se dan en la zona de Manhattan o el Aeropuerto

#Linear color map with steps based on quantiles
colormap2 = cm.linear.Reds_09.to_step(10).scale(0,1)
print(colormap2)

#We display the map
mymap2 = folium.Map(location=[y_map, x_map], zoom_start=11,tiles=None)
folium.TileLayer('CartoDB positron',name="Light Map",control=False).add_to(mymap2)
colormap2.caption = "Taxi Drop Offs"
style_function = lambda x: {"weight":0.5,
                            'color':'black',
                            'fillColor':colormap2(x['properties']['do_norm']),
                            'fillOpacity':0.75}
highlight_function = lambda x: {'fillColor': '#000000',
                                'color':'#000000',
                                'fillOpacity': 0.50,
                                'weight': 0.1}
TaxiPU=folium.features.GeoJson(
        taxi_pu_do,
        style_function=style_function,
        control=False,
        highlight_function=highlight_function,
        tooltip=folium.features.GeoJsonTooltip(fields=['zone','drop_offs'],
            aliases=['Neighbourhood: ','Drop Offs: '],
            style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;"),
            sticky=True
        )
    )
colormap2.add_to(mymap2)
mymap2.add_child(TaxiPU)
embed_map(mymap2, 'mymap2.html')

#Linear color map with steps based on quantiles
colormap3 = cm.linear.RdBu_11.to_step(10).scale(-1,1)
print(colormap3)

#We display the map
mymap3 = folium.Map(location=[y_map, x_map], zoom_start=11,tiles=None)
folium.TileLayer('CartoDB positron',name="Light Map",control=False).add_to(mymap3)
colormap3.caption = "Difference (PU - DO)"
style_function = lambda x: {"weight":0.5,
                            'color':'black',
                            'fillColor':colormap3(x['properties']['diff_norm']),
                            'fillOpacity':0.75}
highlight_function = lambda x: {'fillColor': '#000000',
                                'color':'#000000',
                                'fillOpacity': 0.50,
                                'weight': 0.1}
TaxiPU=folium.features.GeoJson(
        taxi_pu_do,
        style_function=style_function,
        control=False,
        highlight_function=highlight_function,
        tooltip=folium.features.GeoJsonTooltip(fields=['zone','pick_ups','pu_norm','drop_offs','do_norm','diff','diff_norm'],
            aliases=['Neighbourhood: ','Pick Ups: ','Pick Ups Norm: ','Drop Offs: ','Drop Offs Norm: ','Difference: ','Difference Norm: '],
            style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;"),
            sticky=True
        )
    )
colormap3.add_to(mymap3)
mymap3.add_child(TaxiPU)
embed_map(mymap3, 'mymap3.html')

#Origin destination matrix
yellow_taxis_od = yellow_taxis[["PULocationID","tpep_pickup_datetime","DOLocationID","tpep_dropoff_datetime","trip_distance","fare_amount"]]
print(yellow_taxis_od.head())

#Functions to add values to the table
def add_origin_centroid(row) :
    shape = taxi_zones_named.loc[int(row["PULocationID"]), 'geometry']
    return [shape.centroid.x,shape.centroid.y]

def add_origin_zone(row) :
    return taxi_zones_named.loc[int(row["PULocationID"]), 'zone']

def add_destination_centroid(row) :
    shape = taxi_zones_named.loc[int(row["PULocationID"]), 'geometry']
    return [shape.centroid.x,shape.centroid.y]

def add_destination_zone(row) :
    return taxi_zones_named.loc[int(row["DOLocationID"]), 'zone']

from datetime import datetime
def add_travel_time(row) :
    time = (datetime.strptime(row["tpep_dropoff_datetime"], '%Y-%m-%d %H:%M:%S') - datetime.strptime(row["tpep_pickup_datetime"], '%Y-%m-%d %H:%M:%S')).total_seconds()
    return time

#yellow_taxis_od["pu_centroid"] = yellow_taxis_od.apply(add_origin_centroid, axis=1)
#yellow_taxis_od["do_centroid"] = yellow_taxis_od.apply(add_destination_centroid, axis=1)
#yellow_taxis_od["pu_zone"] = yellow_taxis_od.apply(add_origin_zone, axis=1)
#yellow_taxis_od["do_zone"] = yellow_taxis_od.apply(add_destination_zone, axis=1)

yellow_taxis_od_dur = yellow_taxis_od
yellow_taxis_od_dur["duration"] = yellow_taxis_od.apply(add_travel_time, axis=1)
yellow_taxis_od_dur = yellow_taxis_od_dur[["PULocationID","DOLocationID","trip_distance","fare_amount","duration"]]
print(yellow_taxis_od_dur.head())

#Function that prepares the data for the interactive representation
def origin_dest_map(origin_region,df_od) :
    if origin_region.isdigit():
        df = df_od.loc[df_od["PULocationID"] == origin_region][["DOLocationID","trip_distance","fare_amount","duration"]]

    new_df = taxi_zones_named.reset_index().rename(columns={"LocationID": "DOLocationID"})
    new_df["drop_offs"] = new_df.DOLocationID.map(df.DOLocationID.value_counts()).fillna(0)

    new_df["do_norm"] = round(new_df["drop_offs"]/new_df["drop_offs"].max(),3)

    def distance_mean(row) :
        df_dest = df.loc[df["DOLocationID"] == row["DOLocationID"]].fillna(0)
        return round(df_dest.trip_distance.mean(),3)

    new_df["distance_mean"] = new_df.apply(distance_mean, axis=1).fillna(0)

    def fare_amount_mean(row) :
        df_dest = df.loc[df["DOLocationID"] == row["DOLocationID"]]
        return round(df_dest.fare_amount.mean(),3)

    new_df["fare_amount_mean"] = new_df.apply(fare_amount_mean, axis=1).fillna(0)

    def duration_mean(row) :
        df_dest = df.loc[df["DOLocationID"] == row["DOLocationID"]].fillna(0)
        return round(df_dest.duration.mean()/60,3)

    new_df["duration_minutes_mean"] = new_df.apply(duration_mean, axis=1).fillna(0)

    return new_df

def destination_orig_map(origin_region,df_od) :
    if origin_region.isdigit():
        df = df_od.loc[df_od["DOLocationID"] == origin_region][["PULocationID","trip_distance","fare_amount","duration"]]


    new_df = taxi_zones_named.reset_index().rename(columns={"LocationID": "PULocationID"})
    new_df["pick_ups"] = new_df.PULocationID.map(df.PULocationID.value_counts()).fillna(0)

    new_df["pu_norm"] = round(new_df["pick_ups"]/new_df["pick_ups"].max(),3)

    def distance_mean(row) :
        df_dest = df.loc[df["PULocationID"] == row["PULocationID"]].fillna(0)
        return round(df_dest.trip_distance.mean(),3)

    new_df["distance_mean"] = new_df.apply(distance_mean, axis=1).fillna(0)

    def fare_amount_mean(row) :
        df_dest = df.loc[df["PULocationID"] == row["PULocationID"]]
        return round(df_dest.fare_amount.mean(),3)

    new_df["fare_amount_mean"] = new_df.apply(fare_amount_mean, axis=1).fillna(0)

    def duration_mean(row) :
        df_dest = df.loc[df["PULocationID"] == row["PULocationID"]].fillna(0)
        return round(df_dest.duration.mean()/60,3)

    new_df["duration_minutes_mean"] = new_df.apply(duration_mean, axis=1).fillna(0)

    return new_df

origin_row_data = taxi_zones_named.loc[taxi_zones_named.zone == "JFK Airport"]
print(origin_row_data)

#We can plot the main destinations and data with variable origin changing the origin region LocationID
origin_number = "132"
origin_name = taxi_zones_named.loc[origin_number,"zone"]
print(origin_name)

#We create the dataframe
from_df = origin_dest_map(origin_number,yellow_taxis_od_dur)
print(from_df.head())

# Linear color map with steps
colormap4 = cm.linear.Reds_09.to_step(10).scale(0,1)
print(colormap4)

#Now we generate the interactive plot
mymap4 = folium.Map(location=[y_map, x_map], zoom_start=11,tiles=None)
folium.TileLayer('CartoDB positron',name="Light Map",control=False).add_to(mymap4)
colormap4.caption = "Trips from {}".format(origin_name)
style_function = lambda x: {"weight":0.5,
                            'color':'black',
                            'fillColor':colormap4(x['properties']['do_norm']),
                            'fillOpacity':0.75}
highlight_function = lambda x: {'fillColor': '#000000',
                                'color':'#000000',
                                'fillOpacity': 0.50,
                                'weight': 0.1}
TaxiPU=folium.features.GeoJson(
        from_df,
        style_function=style_function,
        control=False,
        highlight_function=highlight_function,
        tooltip=folium.features.GeoJsonTooltip(fields=['zone','drop_offs','do_norm','distance_mean','fare_amount_mean','duration_minutes_mean'],
            aliases=['Neighbourhood: ','Drop Offs: ', 'DO Normalized: ', 'Distance Media en millas: ', 'Tarifa Media en $: ', 'Duracion Media en minutos: '],
            style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;"),
            sticky=True
        )
    )
colormap4.add_to(mymap4)
mymap4.add_child(TaxiPU)
embed_map(mymap4, 'mymap4.html')

destination_row_data = taxi_zones_named.loc[taxi_zones_named.zone == "Times Sq/Theatre District"]
print(destination_row_data)

#We can plot the main origins and data
destination_number = "230"
destination_name = taxi_zones_named.loc[destination_number,"zone"]
print(destination_name)

#We create the dataframe
to_df = destination_orig_map(destination_number,yellow_taxis_od_dur)
print(to_df.head())

# Linear color map with steps
colormap5 = cm.linear.Blues_09.to_step(10).scale(0,1)
print(colormap5)

#Now we generate the interactive plot
mymap5 = folium.Map(location=[y_map, x_map], zoom_start=11,tiles=None)
folium.TileLayer('CartoDB positron',name="Light Map",control=False).add_to(mymap5)
colormap5.caption = "Trips to {}".format(destination_name)
style_function = lambda x: {"weight":0.5,
                            'color':'black',
                            'fillColor':colormap5(x['properties']['pu_norm']),
                            'fillOpacity':0.75}
highlight_function = lambda x: {'fillColor': '#000000',
                                'color':'#000000',
                                'fillOpacity': 0.50,
                                'weight': 0.1}
TaxiPU=folium.features.GeoJson(
        to_df,
        style_function=style_function,
        control=False,
        highlight_function=highlight_function,
        tooltip=folium.features.GeoJsonTooltip(fields=['zone','pick_ups','pu_norm','distance_mean','fare_amount_mean','duration_minutes_mean'],
            aliases=['Neighbourhood: ','Pick Ups: ', 'PU Normalized: ', 'Distance Media en millas: ', 'Tarifa Media en $: ', 'Duracion Media en minutos: '],
            style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;"),
            sticky=True
        )
    )
colormap5.add_to(mymap5)
mymap5.add_child(TaxiPU)
embed_map(mymap5, 'mymap5.html')
